msgid ""
msgstr ""
"Project-Id-Version: telepathy-kde-text-ui\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2016-11-19 20:16+0100\n"
"PO-Revision-Date: 2015-02-10 10:42+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Dominik Schmidt Edmundson Telepathy Nwokeka Francesco\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Smiley kte collaborative Marcin OTR Ziemiński\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "José Nuno Pires"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "zepires@gmail.com"

#: chat-window.cpp:201
#, kde-format
msgid "Close"
msgstr "Fechar"

#: chat-window.cpp:202
#, kde-format
msgid "Detach Tab"
msgstr "Desanexar a Página"

#: chat-window.cpp:203
#, kde-format
msgid "Move Tab Left"
msgstr "Mover a Página para a Esquerda"

#: chat-window.cpp:204
#, kde-format
msgid "Move Tab Right"
msgstr "Mover a Página para a Direita"

#: chat-window.cpp:834
#, kde-format
msgid "&Next Tab"
msgstr "Página Segui&nte"

#: chat-window.cpp:838
#, kde-format
msgid "&Previous Tab"
msgstr "&Página Anterior"

#: chat-window.cpp:842
#, kde-format
msgid "&Audio Call"
msgstr "Ch&amada de Áudio"

#: chat-window.cpp:843
#, kde-format
msgctxt "Toolbar icon tooltip"
msgid "Start an audio call with this contact"
msgstr "Iniciar uma chamada de áudio com este contacto"

#: chat-window.cpp:846 chat-window.cpp:1282
#, kde-format
msgid "&Block Contact"
msgstr "&Bloquear o Contacto"

#: chat-window.cpp:848
#, kde-format
msgctxt "Toolbar icon tooltip"
msgid ""
"Blocking means that this contact will not see you online and you will not "
"receive any messages from this contact"
msgstr ""
"O bloqueio significa que este contacto não o irá ver ligado e não irá "
"receber nenhumas mensagens do mesmo"

#: chat-window.cpp:851
#, kde-format
msgid "&Send File"
msgstr "En&viar o Ficheiro"

#: chat-window.cpp:852
#, kde-format
msgctxt "Toolbar icon tooltip"
msgid "Send a file to this contact"
msgstr "Enviar um ficheiro para este contacto"

#: chat-window.cpp:855
#, kde-format
msgid "&Invite to Chat"
msgstr "Conv&idar para a Conversa"

#: chat-window.cpp:856
#, kde-format
msgctxt "Toolbar icon tooltip"
msgid "Invite another contact to join this chat"
msgstr "Convidar outro contacto para esta conversa"

#: chat-window.cpp:859
#, kde-format
msgid "&Video Call"
msgstr "Chamada de &Vídeo"

#: chat-window.cpp:860
#, kde-format
msgctxt "Toolbar icon tooltip"
msgid "Start a video call with this contact"
msgstr "Iniciar uma chamada de vídeo com este contacto"

#: chat-window.cpp:863
#, kde-format
msgid "Share My &Desktop"
msgstr "Partilhar o Meu &Ecrã"

#: chat-window.cpp:864
#, kde-format
msgctxt "Toolbar icon tooltip"
msgid "Start an application that allows this contact to see your desktop"
msgstr "Iniciar uma aplicação que permite a este contacto ver o seu ecrã"

#: chat-window.cpp:867
#, kde-format
msgid "&Collaboratively edit a document"
msgstr "Editar um documento de forma &colaborativa"

#: chat-window.cpp:870
#, kde-format
msgid "&Contact info"
msgstr "Informação do &contacto"

#: chat-window.cpp:873
#, kde-format
msgid "&Leave room"
msgstr "Sair da &sala"

#: chat-window.cpp:884
#, kde-format
msgid "Choose Spelling Language"
msgstr "Escolher a Língua do Dicionário"

#: chat-window.cpp:886
#, kde-format
msgctxt "Action to open the log viewer with a specified contact"
msgid "&Previous Conversations"
msgstr "&Conversas Anteriores"

#: chat-window.cpp:889
#, kde-format
msgctxt "Action to open the contact list"
msgid "Contact &List"
msgstr "&Lista de Contactos"

#: chat-window.cpp:894
#, kde-format
msgid "Account Icon"
msgstr "Ícone da Conta"

#: chat-window.cpp:898
#, kde-format
msgid "&Clear View"
msgstr "&Limpar a Janela"

#: chat-window.cpp:899
#, kde-format
msgctxt "Toolbar icon tooltip"
msgid "Clear all messages from current chat tab"
msgstr "Limpar todas as mensagens da área de conversação actual"

#: chat-window.cpp:905
#, kde-format
msgid "Send message"
msgstr "Enviar uma mensagem"

#: chat-window.cpp:936
#, kde-format
msgid "&OTR"
msgstr "&OTR"

#: chat-window.cpp:939 chat-window.cpp:1000
#, kde-format
msgid "&Start session"
msgstr "Iniciar a &sessão"

#: chat-window.cpp:943
#, kde-format
msgid "&Stop session"
msgstr "&Parar a sessão"

#: chat-window.cpp:947
#, kde-format
msgid "&Authenticate contact"
msgstr "&Autenticar o contacto"

#: chat-window.cpp:998
#, kde-format
msgid "Not private"
msgstr "Não privado"

#: chat-window.cpp:1007
#, kde-format
msgid "Unverified"
msgstr "Não verificado"

#: chat-window.cpp:1009 chat-window.cpp:1018 chat-window.cpp:1027
#, kde-format
msgid "&Restart session"
msgstr "&Reiniciar a sessão"

#: chat-window.cpp:1016
#, kde-format
msgid "Private"
msgstr "Privado"

#: chat-window.cpp:1025
#, kde-format
msgid "Finished"
msgstr "Terminado"

#: chat-window.cpp:1086
#, kde-format
msgctxt "Toolbar icon tooltip"
msgid "Edit a plain-text document with this contact in real-time"
msgstr "Editar um documento de texto simples com este contacto em tempo-real"

#: chat-window.cpp:1089
#, kde-format
msgctxt "Toolbar icon tooltip for a disabled action"
msgid ""
"<p>Both you and the target contact need to have the <i>kte-collaborative</i> "
"package installed to share documents</p>"
msgstr ""
"<p>Tanto você como o contacto de destino precisam de ter o pacote <i>kte-"
"collaborative</i> instalado para partilhar documentos</p>"

#: chat-window.cpp:1188
#, kde-format
msgid "Choose files to send to %1"
msgstr "Escolher os ficheiros a enviar para %1"

#: chat-window.cpp:1275
#, kde-format
msgid "&Unblock Contact"
msgstr "Des&bloquear o Contacto"

#. i18n: ectx: Menu (conversation)
#: chatwindow.rc:11
#, kde-format
msgid "&Conversation"
msgstr "&Conversação"

#. i18n: ectx: Menu (actions)
#: chatwindow.rc:24
#, kde-format
msgid "&Actions"
msgstr "&Acções"

#. i18n: ectx: ToolBar (mainToolBar)
#: chatwindow.rc:41
#, kde-format
msgid "Main Toolbar"
msgstr "Barra Principal"

#. i18n: ectx: ToolBar (accountIconToolBar)
#: chatwindow.rc:53
#, kde-format
msgid "Account Icon Toolbar"
msgstr "Barra do Ícone da Conta"

#. i18n: ectx: ToolBar (languageToolBar)
#: chatwindow.rc:58
#, kde-format
msgid "Language Toolbar"
msgstr "Barra da Língua"

#. i18n: ectx: ToolBar (emoticonsToolBar)
#: chatwindow.rc:63
#, kde-format
msgid "Smiley Toolbar"
msgstr "Barra de Ícones Emotivos"

#: emoticon-text-edit-action.cpp:51
#, kde-format
msgid "Add Smiley"
msgstr "Adicionar Smiley"

#: invite-contact-dialog.cpp:55
#, kde-format
msgid "Search in Contacts..."
msgstr "Procurar nos Contactos..."

#: invite-contact-dialog.cpp:64
#, kde-format
msgid "Select Contacts to Invite to Group Chat"
msgstr "Seleccionar os Contactos a Convidar para a Conversa em Grupo"

#: telepathy-chat-ui.cpp:62
#, kde-format
msgid "Chat Application"
msgstr "Aplicação de Conversação"

#: telepathy-chat-ui.cpp:63
#, kde-format
msgid "David Edmundson"
msgstr "David Edmundson"

#: telepathy-chat-ui.cpp:63 telepathy-chat-ui.cpp:64
#, kde-format
msgid "Developer"
msgstr "Desenvolvimento"

#: telepathy-chat-ui.cpp:64
#, kde-format
msgid "Marcin Ziemiński"
msgstr "Marcin Ziemiński"

#: telepathy-chat-ui.cpp:65
#, kde-format
msgid "Dominik Schmidt"
msgstr "Dominik Schmidt"

#: telepathy-chat-ui.cpp:65 telepathy-chat-ui.cpp:66
#, kde-format
msgid "Past Developer"
msgstr "Desenvolvimento Anterior"

#: telepathy-chat-ui.cpp:66
#, kde-format
msgid "Francesco Nwokeka"
msgstr "Francesco Nwokeka"
