# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sairan Kikkarin <sairan@computer.org>, 2012, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2016-11-19 20:16+0100\n"
"PO-Revision-Date: 2013-09-25 03:42+0600\n"
"Last-Translator: Sairan Kikkarin <sairan@computer.org>\n"
"Language-Team: Kazakh <kde-i18n-doc@kde.org>\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: behavior-config.cpp:50
#, kde-format
msgctxt ""
"Part of config 'show last [spin box] messages' This is the suffix to the "
"spin box. Be sure to include leading space"
msgid " message"
msgid_plural " messages"
msgstr[0] " хабарлама"

#: behavior-config.cpp:67
#, kde-format
msgctxt "Placeholder for contact name in completion suffix selector"
msgid "Nickname"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, newTabGroupBox)
#: behavior-config.ui:17
#, kde-format
msgid "Tabs"
msgstr "Қойындылар"

#. i18n: ectx: property (text), widget (QLabel, label)
#: behavior-config.ui:23
#, kde-format
msgid "Open new conversations:"
msgstr "Жаңа әңгімелерді бастау:"

#. i18n: ectx: property (text), widget (QRadioButton, radioNew)
#: behavior-config.ui:30
#, kde-format
msgid "As new windows"
msgstr "Жаңа терезеде"

#. i18n: ectx: property (text), widget (QRadioButton, radioZero)
#: behavior-config.ui:40
#, kde-format
msgid "As tabs in the same window"
msgstr "Терезедегі жаңа қойынды ретінде"

#. i18n: ectx: property (title), widget (QGroupBox, scrollbackGroupBox)
#: behavior-config.ui:53
#, kde-format
msgid "Last Conversation Scrollback"
msgstr "Соңғы әңгіменің хаттамасы"

#. i18n: First part of &quot;Show last %1 messages&quot; string
#. i18n: ectx: property (text), widget (QLabel, label_p1)
#: behavior-config.ui:59
#, kde-format
msgid "Show last"
msgstr "Соңғысын көрсету"

#. i18n: ectx: property (title), widget (QGroupBox, typingGroupBox)
#: behavior-config.ui:85
#, kde-format
msgid "User Is Typing"
msgstr "Пайдаланушы теріп жатыр"

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxShowOthersTyping)
#: behavior-config.ui:91
#, kde-format
msgid "Show me when others are typing"
msgstr "Басқалар теріп жатқанын маған көрсету"

#. i18n: ectx: property (text), widget (QCheckBox, checkBoxShowMeTyping)
#: behavior-config.ui:98
#, kde-format
msgid "Show others when I am typing"
msgstr "Мен теріп жатқанын басқаларға көрсету"

#. i18n: ectx: property (title), widget (QGroupBox, nicknameCompletionGroup)
#: behavior-config.ui:108
#, kde-format
msgid "Nickname Completion"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: behavior-config.ui:117
#, kde-format
msgid "Complete nicknames to"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: behavior-config.ui:137
#, kde-format
msgid "when I press Tab"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, kbuttongroup)
#: behavior-config.ui:147
#, kde-format
msgid "Image Sharing"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: behavior-config.ui:156
#, kde-format
msgid "Image Sharing Service"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: behavior-config.ui:179
#, kde-format
msgid "Works when dragging an image to the Chat Window"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, groupChatsGroup)
#: behavior-config.ui:195
#, kde-format
msgid "Group Chats"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, dontLeaveGroupChats)
#: behavior-config.ui:207
#, kde-format
msgid "Stay connected to group chat even when tab is closed"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: behavior-config.ui:217
#, kde-format
msgid "Keyboard Layouts"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, rememberTabKeyboardLayout)
#: behavior-config.ui:223
#, kde-format
msgid "Remember keyboard layout for each tab"
msgstr ""

#~ msgid "A new tab in the last focused window"
#~ msgstr "Соңғы назарда болған терезеде жаңа қойынды"
